from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# database models for the relpol app


class MainTopic(models.Model):
    """List of main topics:
    Friends and Family
    Dating
    Work and Business"""
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "Main Topic: " + self.name



class SubTopic(models.Model):
    main_topic = models.ForeignKey(MainTopic)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "Sub Topic: " + self.name


class Picture(models.Model):
    picture_file = models.FilePathField(path="/home/yuhwei/RelPol/relpol/relpolapp/static/relpolapp/pictures/", max_length=200)

    def __unicode__(self):
        return "Picture filepath: " + self.picture_file

class BlogPost(models.Model):
    name = models.CharField(max_length=200)
    keyword1 = models.CharField(max_length=200)
    keyword2 = models.CharField(max_length=200)
    keyword3 = models.CharField(max_length=200)
    content_file = models.FilePathField(path="/home/yuhwei/RelPol/relpol/relpolapp/static/relpolapp/blogposts/", max_length=200)
    sub_topic = models.ForeignKey(SubTopic)
    picture = models.ForeignKey(Picture)


    def __unicode__(self):
        return "Blog Post: " + self.name + " Path: " + self.content_file


class FeaturedArticle(models.Model):
    blog_post = models.ForeignKey(BlogPost)


    def __unicode__(self):
        return "Featured Article: " + str(self.blog_post)


class Advertisement(models.Model):
    link = models.URLField(max_length=200)
    picture = models.ForeignKey(Picture, on_delete=models.CASCADE)

    def __unicode__(self):
        return "Advertisement: " + self.link



class Comment(models.Model):
    post_datetime = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=500)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="commenter")
    blog_post = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    in_reply_to = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="inreplyto")


    def __unicode__(self):
        return "Comment by userId: " + str(self.user) + "\n" + comment 



