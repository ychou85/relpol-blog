from django.contrib import admin
from .models import *

admin.site.register(MainTopic)
admin.site.register(SubTopic)
admin.site.register(Picture)
admin.site.register(BlogPost)
admin.site.register(FeaturedArticle)
admin.site.register(Advertisement)
admin.site.register(Comment)
