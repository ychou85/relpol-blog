from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.edit import FormView
#from django.core.urlresolvers import reverse_lazy

from . import myforms


def index(request):
    return render(request, 'relpolapp/index.html')


class LoginView(FormView):
    template_name = 'relpolapp/login.html'
    form_class = myforms.LoginForm
    success_url = '/relpolapp'


class SignUpView(FormView):
    template_name = 'relpolapp/signup.html'
    form_class = myforms.SignUpForm
    success_url = '/relpolapp'


class ForgotCredentialsView(FormView):
    template_name = 'relpolapp/forgot.html'
    form_class = myforms.ForgotCredentialsForm
    success_url = '/relpolapp'
    

