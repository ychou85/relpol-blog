from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.db.models import Q



class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label='Password', max_length=60, widget=forms.PasswordInput)

    def is_valid(self):
        valid = super(LoginForm, self).is_valid()

        if not valid:
            return valid
        
        try:
            user = User.objects.get(Q(username=self.cleaned_data['username']) | Q(email=self.cleaned_data['username']))

        except User.DoesNotExist:    
            self._errors['user_not_found'] = 'Username could not be found!'
            return False

        if not check_password(self.cleaned_data['password'], user.password):
            self._errors['password_incorrect'] = 'Password entered is incorrect for this account!'
            return False

        return True


class SignUpForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label='Password', max_length=60, widget=forms.PasswordInput)
    re_password = forms.CharField(label='Re-Enter Password', max_length=60, widget=forms.PasswordInput)
    email = forms.CharField(label='Email', max_length=254)
    confirm_email = forms.CharField(label='Re-Enter Email', max_length=254)

    def is_valid(self):
        valid = super(SignUpForm, self).is_valid()

        if not valid:
            return valid

        if (self.cleaned_data['password'] != self.cleaned_data['re_password']):
            self._errors['password_mismatch'] = 'Your passwords do not match!'
            return False

        if (self.cleaned_data['email'] != self.cleaned_data['confirm_email']):
            self._errors['email_mismatch'] = 'Your emails do not match!'
            return False

        return True



class ForgotCredentialsForm(forms.Form):
    email = forms.CharField(label='Email', max_length=254)
    
    
    



