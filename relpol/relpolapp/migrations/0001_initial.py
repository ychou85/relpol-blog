# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Advertisement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('keyword1', models.CharField(max_length=200)),
                ('keyword2', models.CharField(max_length=200)),
                ('keyword3', models.CharField(max_length=200)),
                ('content_file', models.FilePathField(path=b'/home/yuhwei/RelPol/relpol/relpolapp/static/relpolapp/blogposts/', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_datetime', models.DateTimeField(auto_now_add=True)),
                ('comment', models.CharField(max_length=500)),
                ('blog_post', models.ForeignKey(to='relpolapp.BlogPost')),
                ('in_reply_to', models.ForeignKey(related_name='inreplyto', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(related_name='commenter', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='FeaturedArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('blog_post', models.ForeignKey(to='relpolapp.BlogPost')),
            ],
        ),
        migrations.CreateModel(
            name='MainTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('picture_file', models.FilePathField(path=b'/home/yuhwei/RelPol/relpol/relpolapp/static/relpolapp/pictures/', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='SubTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('main_topic', models.ForeignKey(to='relpolapp.MainTopic')),
            ],
        ),
        migrations.AddField(
            model_name='blogpost',
            name='picture',
            field=models.ForeignKey(to='relpolapp.Picture'),
        ),
        migrations.AddField(
            model_name='blogpost',
            name='sub_topic',
            field=models.ForeignKey(to='relpolapp.SubTopic'),
        ),
        migrations.AddField(
            model_name='advertisement',
            name='picture',
            field=models.ForeignKey(to='relpolapp.Picture'),
        ),
    ]
