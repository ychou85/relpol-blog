from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login', views.LoginView.as_view(), name='login'),
    url(r'^signup', views.SignUpView.as_view(), name='signup'),
    url(r'^forgot-password', views.ForgotCredentialsView.as_view(), name='forgot_password'),
    url(r'^forgot-username', views.ForgotCredentialsView.as_view(), name='forgot_username'),
]
